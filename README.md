# 병원 방문 등록 조회 서비스

### 0. 기술 스텍
<img src="https://img.shields.io/badge/SpringBoot-6DB33F?style=for-the-badge&logo=SpringBoot&logoColor=white">

### 1. 기능 정의

- 로그인 한 사용자가 본인이 방문한 병원에 증상, 진료비 등 등록
- 전체, 특정 user, 특정 병원 별로 방문 기록을 조회
- User - 로그인, 회원가입
- Visitor - 방문 등록, 조회
- Hospital - 11만개의 병원 데이터 조회

### 2. API 설계

| HTTP | EndPoint                      | 기능               |
| --- |-------------------------------|------------------|
| POST | /api/v1/users/join            | 회원가입 기능          |
| POST | /api/v1/users/login           | 로그인 기능, Token발급  |
| POST | /api/v1/visits                | create기능, Token인증 |
| GET | /api/v1/visits                | 전체 조회            |
| GET | /api/v1/visits/users/{id}     | 특정 user의 기록 조회   |
| GET | /api/v1/visits/hospitals/{id} | 특정 병원의 기록 조회     |

### 3. 다이어그램 설계

![img.png](img.png)