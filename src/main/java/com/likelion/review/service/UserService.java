package com.likelion.review.service;

import com.likelion.review.domain.User;
import com.likelion.review.domain.dto.UserDto;
import com.likelion.review.domain.dto.UserJoinRequest;
import com.likelion.review.exception.AppException;
import com.likelion.review.exception.ErrorCode;
import com.likelion.review.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;
    public UserDto join(UserJoinRequest userJoinRequest){

        // 회원 중복 확인
        userRepository.findByUserName(userJoinRequest.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, String.format("Username:%s",userJoinRequest.getUserName()));
                });

        // 회원 가입
        User savedUser = userRepository.save(userJoinRequest.toEntity(encoder.encode(userJoinRequest.getPassword())));
        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .password(savedUser.getPassword())
                .emailAddress(savedUser.getEmailAddress())
                .build();
    }
}
